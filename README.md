#TextJuggler

a simple example for using object-oriented abstractions.

There are 4 different implementations of the same functionality in this project:

##package `procedural`
A very basic procedurally scripted implementation. The entire functionality is contained in a single class.

##package `basic`
A basic implementation based on simple inheritance (without abstract classes). 
This version chooses the super classes for operations and io-handlers arbitrarily.

##package `abstraction`
Introduces an abstract class AbstractOperation as the common base class for all operation implementations.

##package `intf`
Supplements the abstract implementation of InputOutputHandler with an interface.
