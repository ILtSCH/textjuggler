/*
 * Copyright (c) 2019.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package streaming;

import java.io.PrintWriter;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class TextJuggler {

    private Stream<String> in;
    private Function<String, String> mapper;
    private PrintWriter out;

    public TextJuggler(Function<String, String> mapper, Stream<String> in, PrintWriter out) {
        this.mapper = mapper;
        this.in = in;
        this.out = out;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String... args) {
        parseArgs(args).ifPresentOrElse(TextJuggler::juggle, TextJuggler::usage);
    }

    private static void usage() {
        System.out.println("usage: TextJuggler operation input output");
    }

    /**
     * parse program arguments.
     *
     * @return an Optional containing a TextJuggler instance if arguments could be parsed successfully, an empty Optional otherwise.
     */
    private static Optional<TextJuggler> parseArgs(final String[] args) {
        if (args.length != 3) {
            return Optional.empty();
        }
        TextJuggler textJuggler = new TextJuggler(
                Operations.valueOf(args[0]).getMapper(),
                DataStore.valueOf(args[1]).getStream(),
                DataStore.valueOf(args[2]).getWriter());
        return Optional.of(textJuggler);
    }

    /**
     * reads from in. transforms each input line according to op. writes to out.
     */
    public void juggle() {
        try (PrintWriter localOut = out) {
            in.map(mapper).forEach(out::println);
        }
    }
}
