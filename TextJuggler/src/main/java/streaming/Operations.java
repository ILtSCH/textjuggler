/*
 * Copyright (c) 2019.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package streaming;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * defines the word/line operations supported by the application.
 *
 * @author ILtSCH
 */
public enum Operations {

    /**
     * reverse the words in the line
     */
    REVERSE(Collections::reverse),
    /**
     * sort the words in the line in ascending order
     */
    SORT(Collections::sort),
    /**
     * shuffle the words in the line
     */
    SHUFFLE(Collections::shuffle),

    TRUNCATE(Operations::truncate);

    private Consumer<List<String>> listOperator;

    Operations(Consumer<List<String>> listOperator) {
        this.listOperator = listOperator;
    }

    private static void truncate(List<String> words) {
        for (int i = 0; i < words.size(); ++i) {
            words.set(i, truncate(words.get(i)));
        }
    }

    private static String truncate(String s) {
        return s.length() < 4 ? s : s.substring(0, 4);
    }

    public Function<String, String> getMapper() {
        return this::processString;
    }

    private String processString(String s) {
        List<String> words = Arrays.asList(s.split("(\\s|,)+"));
        listOperator.accept(words);
        return String.join(" ", words);
    }
}
