/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package streaming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;


/**
 * defines the input/output sources/targets supported by the application.
 *
 * @author ILtSCH
 */
public enum DataStore {

    /**
     * read/write from/to console/terminal (i.e. stdin/stdout)
     */
    CONSOLE(() -> new BufferedReader(new InputStreamReader(System.in)), () -> new PrintWriter(System.out)),
    /**
     * read/write from/to file.
     */
    FILE(DataStore::getFileReader, DataStore::getFileWriter);


    private Supplier<PrintWriter> writerSupplier;
    private Supplier<BufferedReader> readerSupplier;

    DataStore(Supplier<BufferedReader> readerSupplier, Supplier<PrintWriter> writerSupplier) {
        this.readerSupplier = readerSupplier;
        this.writerSupplier = writerSupplier;
    }

    private static PrintWriter getFileWriter() {
        try {
            return new PrintWriter(Files.newBufferedWriter(Paths.get("output.txt")));
        } catch (Exception e) {
            throw new DataStoreException(e);
        }
    }

    private static BufferedReader getFileReader() {
        try {
            return Files.newBufferedReader(Paths.get(TextJuggler.class.getResource("input.txt").toURI()));
        } catch (Exception e) {
            throw new DataStoreException(e);
        }
    }

    private static String readLine(BufferedReader reader) {
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new DataStoreException(e);
        }
    }

    public PrintWriter getWriter() {
        return writerSupplier.get();
    }

    public Stream<String> getStream() {
        BufferedReader reader = readerSupplier.get();
        return Stream.generate(() -> readLine(reader)).takeWhile(Objects::nonNull);
    }

    private static class DataStoreException extends RuntimeException {
        public DataStoreException(Exception e) {
            super(e);
        }
    }
}
