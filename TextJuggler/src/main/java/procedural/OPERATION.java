/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package procedural;

/**
 * defines the word/line operations supported by the application.
 *
 * @author ILtSCH
 */
public enum OPERATION {

    /**
     * reverse the words in the line
     */
    REVERSE,
    /**
     * sort the words in the line in ascending order
     */
    SORT,
    /**
     * shuffle the words in the line
     */
    SHUFFLE
}
