/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package procedural;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author ILtSCH
 */
public final class TextJuggler {

    private static OPERATION op;
    private static DATASTORE in;
    private static DATASTORE out;

    private static BufferedReader input = null;
    private static PrintStream output = null;

    /**
     * dummy constructor (static class)
     */
    private TextJuggler() {
    }

    /**
     * @param args the command line arguments
     * @throws java.io.IOException may throw an IOException if something goes wrong
     */
    public static void main(final String[] args) throws IOException {

        if (parseArgs(args)) {
            juggle();
        }
    }

    /**
     * parse program arguments.
     *
     * @return true if arguments could be parsed successfully, false otherwise.
     */
    private static boolean parseArgs(final String[] args) {
        boolean argsOk = true;
        if (args.length != 3) {
            System.out.println("usage: TextJuggler operation input output");
            argsOk = false;
        }
        if (argsOk) {
            op = OPERATION.valueOf(args[0]);
            in = DATASTORE.valueOf(args[1]);
            out = DATASTORE.valueOf(args[2]);
        }
        return argsOk;
    }

    /**
     * reads from in. transforms each input line according to op. writes to out.
     */
    private static void juggle() throws IOException {
        for (String line = read(); line != null && !line.isEmpty(); line = read()) {
            write(process(line));
        }
    }

    private static String read() throws IOException {
        if (input == null) {
            if (in == DATASTORE.CONSOLE) {
                input = new BufferedReader(new InputStreamReader(System.in));
            } else if (in == DATASTORE.FILE) {
                final Charset defaultCharset = Charset.defaultCharset();
                System.out.println("default charset: " + defaultCharset.displayName());
                System.out.printf("file.encoding: %s%n%n", System.getProperty("file.encoding"));
                input = new BufferedReader(new InputStreamReader(new FileInputStream("input.txt"), defaultCharset));
            }
        }
        return input.readLine();
    }

    private static String process(String line) {
        List<String> words = Arrays.asList(line.split("\\s|,"));
        switch (op) {
            case REVERSE:
                Collections.reverse(words);
                break;
            case SORT:
                Collections.sort(words);
                break;
            case SHUFFLE:
                Collections.shuffle(words);
                break;
        }
        StringBuilder ret = new StringBuilder();
        for (String s : words) {
            ret.append(s).append(" ");
        }
        return ret.toString();
    }

    private static void write(String line) throws IOException {
        if (output == null) {
            switch (out) {
                case FILE:
                    output = new PrintStream(new File("output.txt"));
                case CONSOLE:
                    output = System.out;
                default:
                    throw new IllegalArgumentException("unhandled output type");
            }
        }
        output.println(line);
    }
}
