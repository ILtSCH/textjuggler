/*
 * Copyright (c) 2019.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package procedural;

/**
 * defines the input/output sources/targets supported by the application.
 *
 * @author ILtSCH
 */
public enum DATASTORE {

    /**
     * read/write from/to console/terminal (i.e. stdin/stdout)
     */
    CONSOLE,
    /**
     * read/write from/to file.
     */
    FILE

}
