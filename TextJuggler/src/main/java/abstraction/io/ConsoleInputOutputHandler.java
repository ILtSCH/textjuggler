/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package abstraction.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ConsoleInputOutputHandler extends InputOutputHandler {

    public ConsoleInputOutputHandler() {
        super(new BufferedReader(new InputStreamReader(System.in)), System.out);
    }

}
