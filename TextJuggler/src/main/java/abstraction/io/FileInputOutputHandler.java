/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package abstraction.io;

import java.io.*;
import java.nio.charset.Charset;

public class FileInputOutputHandler extends InputOutputHandler {

    public FileInputOutputHandler() throws FileNotFoundException {
        super(new BufferedReader(new InputStreamReader(new FileInputStream("input.txt"), Charset.defaultCharset())),
                new PrintStream(new File("output.txt")));
    }

}
