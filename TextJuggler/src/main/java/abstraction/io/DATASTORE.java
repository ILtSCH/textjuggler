/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package abstraction.io;

/**
 * defines the input/output sources/targets supported by the application.
 *
 * @author ILtSCH
 */
public enum DATASTORE {
    /**
     * read/write from/to console/terminal (i.e. stdin/stdout)
     */
    CONSOLE(ConsoleInputOutputHandler.class),
    /**
     * read/write from/to file.
     */
    FILE(FileInputOutputHandler.class);

    private final Class<? extends InputOutputHandler> handlerClass;

    <T extends InputOutputHandler> DATASTORE(Class<T> handlerClass) {
        this.handlerClass = handlerClass;
    }

    public static InputOutputHandler getInstance(String s) {
        return DATASTORE.valueOf(s).getHandler();
    }

    public InputOutputHandler getHandler() {
        try {
            return handlerClass.getConstructor().newInstance();
        } catch (Exception e) {
            throw new IllegalStateException("unable to instantiate input/output handler");
        }
    }

}
