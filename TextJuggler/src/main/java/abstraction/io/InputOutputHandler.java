/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package abstraction.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

public abstract class InputOutputHandler {

    private final BufferedReader inputReader;
    private final PrintStream output;

    protected InputOutputHandler(BufferedReader input, PrintStream output) {
        this.inputReader = input;
        this.output = output;
    }

    public String read() throws IOException {
        return inputReader.readLine();
    }

    public void write(String s) {
        output.println(s);
    }
}
