/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package abstraction.operations;

import java.util.Collections;
import java.util.List;

public class SortOperation extends AbstractOperation {

    @Override
    protected void execute(List<String> words) {
        Collections.sort(words);
    }

}
