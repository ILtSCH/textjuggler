/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package abstraction.operations;

/**
 * defines the word/line operations supported by the application.
 *
 * @author ILtSCH
 */
public enum OPERATION {

    /**
     * reverse the words in the line
     */
    REVERSE(new ReverseOperation()),
    /**
     * sort the words in the line in ascending order
     */
    SORT(new SortOperation()),
    /**
     * shuffle the words in the line
     */
    SHUFFLE(new ShuffleOperation());
    /**
     * use only first letters of each word
     */

    private final AbstractOperation handler;

    OPERATION(AbstractOperation operationHandler) {
        this.handler = operationHandler;
    }

    public AbstractOperation getHandler() {
        return handler;
    }
}
