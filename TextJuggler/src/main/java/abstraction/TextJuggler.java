/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package abstraction;

import abstraction.io.DATASTORE;
import abstraction.io.InputOutputHandler;
import abstraction.operations.AbstractOperation;
import abstraction.operations.OPERATION;

import java.io.IOException;

/**
 * @author ILtSCH
 */
public final class TextJuggler {

    private static AbstractOperation op;
    private static InputOutputHandler in;
    private static InputOutputHandler out;

    /**
     * dummy constructor (static class)
     */
    private TextJuggler() {
    }

    /**
     * @param args the command line arguments
     * @throws IOException may throw an IOException if something goes wrong
     */
    public static void main(final String[] args) throws IOException {
        if (parseArgs(args)) {
            juggle();
        }
    }

    /**
     * parse program arguments.
     *
     * @return true if arguments could be parsed successfully, false otherwise.
     */
    private static boolean parseArgs(final String[] args) {
        boolean argsOk = true;
        if (args.length != 3) {
            System.out.println("usage: TextJuggler operation input output");
            argsOk = false;
        }
        if (argsOk) {
            op = OPERATION.valueOf(args[0]).getHandler();
            in = DATASTORE.getInstance(args[1]);
            out = DATASTORE.getInstance(args[2]);
        }
        return argsOk;
    }

    /**
     * reads from in. transforms each input line according to op. writes to out.
     */
    private static void juggle() throws IOException {
        for (String line = in.read(); line != null && !line.isEmpty(); line = in.read()) {
            out.write(op.process(line));
        }
    }
}
