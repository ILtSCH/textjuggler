/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package basic.io;

/**
 * defines the input/output sources/targets supported by the application.
 *
 * @author ILtSCH
 */
public enum DATASTORE {

    /**
     * read/write from/to console/terminal (i.e. stdin/stdout)
     */
    CONSOLE(new InputOutputHandler()),
    /**
     * read/write from/to file.
     */
    FILE(new FileInputOutputHandler());

    private final InputOutputHandler handler;

    DATASTORE(InputOutputHandler handler) {
        this.handler = handler;
    }

    public InputOutputHandler getHandler() {
        return handler;
    }
}
