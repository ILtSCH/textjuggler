/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package basic.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class InputOutputHandler {

    protected BufferedReader inputReader;
    protected PrintStream output;

    public InputOutputHandler() {
        init();
    }

    protected void init() {
        try {
            inputReader = new BufferedReader(new InputStreamReader(System.in));
            output = System.out;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String read() throws IOException {
        return inputReader.readLine();
    }

    public void write(String s) {
        output.println(s);
    }

}
