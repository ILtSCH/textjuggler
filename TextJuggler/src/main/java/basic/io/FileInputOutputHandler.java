/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package basic.io;

import java.io.*;
import java.nio.charset.Charset;

public class FileInputOutputHandler extends InputOutputHandler {

    public FileInputOutputHandler() {
        super();
    }

    @Override
    protected void init() {
        try {
            final Charset defaultCharset = Charset.defaultCharset();
            inputReader = new BufferedReader(new InputStreamReader(new FileInputStream("input.txt"), defaultCharset));
            output = new PrintStream(new File("output.txt"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
