/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package basic;

import basic.io.DATASTORE;
import basic.io.InputOutputHandler;
import basic.operations.OPERATION;
import basic.operations.SortOperation;

import java.io.IOException;

/**
 * @author ILtSCH
 */
public final class TextJuggler {

    private static SortOperation op;
    private static InputOutputHandler in;
    private static InputOutputHandler out;

    /**
     * dummy constructor (static class)
     */
    private TextJuggler() {
    }

    /**
     * @param args the command line arguments
     * @throws IOException may throw an IOException if something goes wrong
     */
    public static void main(final String[] args) throws IOException {
        if (parseArgs(args)) {
            juggle();
        }
    }

    /**
     * parse program arguments.
     * Initialization fails if the file input.txt does not exist even if it is never used!
     *
     * @return true if arguments could be parsed successfully, false otherwise.
     */
    private static boolean parseArgs(final String[] args) {
        boolean argsOk = true;
        if (args.length != 3) {
            System.out.println("usage: TextJuggler operation input output");
            argsOk = false;
        }
        if (argsOk) {
            op = OPERATION.valueOf(args[0]).getHandler();
            in = DATASTORE.valueOf(args[1]).getHandler();
            out = DATASTORE.valueOf(args[2]).getHandler();
        }
        return argsOk;
    }

    /**
     * reads from in. transforms each input line according to op. writes to out.
     */
    private static void juggle() throws IOException {
        for (String line = in.read(); line != null && !line.isEmpty(); line = in.read()) {
            out.write(op.process(line));
        }
    }
}
