/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package basic.operations;

/**
 * defines the word/line operations supported by the application.
 *
 * @author ILtSCH
 */
public enum OPERATION {

    /**
     * reverse the words in the line
     */
    REVERSE(new ReverseOperation()),
    /**
     * sort the words in the line in ascending order
     */
    SORT(new SortOperation()),
    /**
     * shuffle the words in the line
     */
    SHUFFLE(new ShuffleOperation());

    private final SortOperation handler;

    OPERATION(SortOperation operationHandler) {
        this.handler = operationHandler;
    }

    public SortOperation getHandler() {
        return handler;
    }
}
