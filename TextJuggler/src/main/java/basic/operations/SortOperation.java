/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basic.operations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author ILtSCH
 */
public class SortOperation {

    public String process(String line) {
        List<String> words = Arrays.asList(line.split("\\s|,"));
        execute(words);
        StringBuilder ret = new StringBuilder();
        for (String s : words) {
            ret.append(s).append(" ");
        }
        return ret.toString();
    }

    protected void execute(List<String> words) {
        Collections.sort(words);
    }
}
