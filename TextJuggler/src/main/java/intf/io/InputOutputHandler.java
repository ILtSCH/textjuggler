/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package intf.io;

import java.io.IOException;

public interface InputOutputHandler {

    String read() throws IOException;

    void write(String s);
}
