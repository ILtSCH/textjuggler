package streaming;

import org.junit.jupiter.api.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.assertTrue;

class TextJugglerTest {

    @Test
    void testMain() {
        TextJuggler.main("REVERSE", "FILE", "CONSOLE");
    }

    @Test
    void testJuggler() {
        StringWriter out = new StringWriter();
        new TextJuggler(Operations.SORT.getMapper(), DataStore.FILE.getStream(), new PrintWriter(out)).juggle();
        assertTrue(out.toString().startsWith(
                "Hallo Welt bin hier ich!\n" +
                        "Immer aber bin hier ich lange. mehr nicht noch\n" +
                        "Auf Schön Wiedersehen! wars.\n"));
    }
}