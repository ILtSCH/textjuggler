/*
 * Copyright (c) 2018.
 * BEER-WARE LICENSE (Revision 42)
 * This file was created by bitbucket.org/iltsch
 * use it if you find it useful. Beer is appreciated.
 */

package intf.io;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FileInputOutputHandlerTest {

    public static final String LINE_1 = "line 1";
    public static final String LINE_2 = "line 2";
    private static final Path input = Paths.get("input.txt");
    private static final Path output = Paths.get("output.txt");
    private static FileInputOutputHandler ioHandler;

    @BeforeAll
    static void setup() throws Exception {
        try (PrintWriter printWriter = new PrintWriter(input.toFile())) {
            printWriter.println(LINE_1);
            printWriter.println(LINE_2);
        }
        ioHandler = new FileInputOutputHandler();
    }

    @AfterAll
    static void tearDown() throws IOException {
        Files.delete(input);
    }

    @Test
    void read() throws Exception {
        String line = ioHandler.read();
        assertEquals(LINE_1, line);
        assertEquals(LINE_2, ioHandler.read());
        assertEquals("hallo", ioHandler.read(), "line 3 failed!");
    }

    @Test
    void write() throws IOException {
        ioHandler.write(LINE_1);
        ioHandler.write(LINE_2);
        assertTrue(Files.exists(output));
        BufferedReader br = new BufferedReader(new FileReader(output.toFile()));
        assertEquals(LINE_1, br.readLine());
        assertEquals(LINE_2, br.readLine());
    }
}